package bl;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import at.petritzdesigns.bitbot.networking.BitBotClient;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.CommandDataFactory;
import at.petritzdesigns.bitbot.networking.data.commands.ControlMotorCommand;
import at.petritzdesigns.bitbot.networking.data.commands.EstablishConnectionCommand;
import at.petritzdesigns.bitbot.networking.data.commands.PanicCommand;
import at.petritzdesigns.bitbot.utility.Ipv4AddressHelper;
import enums.ForwardBackwardDirection;
import enums.ScreenOrientation;
import enums.UsualMoveDirection;
import movecommand.GyroMoveCommand;
import movecommand.MoveCommand;
import movecommand.StopMoveCommand;
import movecommand.UsualMoveCommand;

/**
 * Created by Jonathan Resch on 04.12.2015.
 *
 * Class Sender
 * Class Sender is the main Business-Logic class of the project.
 * It can establish connection as well as end connections
 * and it's also the base for all movement-command methods,
 * which get called by the event listeners
 */
public class Sender {
    private String ip;
    private int port;
    private BitBotClient client;
    private CommandDataFactory dataFactory;

    private MoveCommand mc= null;

    public Sender(String ip, int port){
        this.ip=ip;
        this.port=port;
    }

    //Connect Methods
    public boolean connect(){
        if (!Ipv4AddressHelper.validate(ip)) {
            return false;
        }
        listenToServer(ip);
        return true;//Status for successfully connected
    }

    public boolean disconnect(){
        stopListening();
        client=null;
        return false;//Status for successfully disconnected
    }

    //Move Methods - Default move
    public boolean forward(int speedleft, int speedright) {
        try{
            mc=new UsualMoveCommand(speedleft,speedright, UsualMoveDirection.FORWARD);
            client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
            sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        }catch (Exception e){
            Log.d("Could not move: ",e.getMessage());
        }
        return true;
    }

    public boolean backward(int speedleft, int speedright) {
        try{
            mc=new UsualMoveCommand(speedleft,speedright, UsualMoveDirection.BACKWARD);
            client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
            sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        }catch (Exception e){
            Log.d("Could not move: ",e.getMessage());
        }
        return true;
    }

    public boolean left(int speedleft, int speedright) {
        try{
            mc=new UsualMoveCommand(speedleft,speedright, UsualMoveDirection.LEFT);
            client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
            sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        }catch (Exception e){
            Log.d("Could not move: ",e.getMessage());
        }
        return true;
    }

    public boolean right(int speedleft, int speedright) {
        try{
            mc=new UsualMoveCommand(speedleft,speedright, UsualMoveDirection.RIGHT);
            client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
            sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        }catch (Exception e){
            Log.d("Could not move: ",e.getMessage());
        }
        return true;
    }

    public boolean leftforward(int speedleft, int speedright) {
        try{
            mc=new UsualMoveCommand(speedleft,speedright, UsualMoveDirection.LFORWARD);
            client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
            sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        }catch (Exception e){
            Log.d("Could not move: ",e.getMessage());
        }
        return true;
    }

    public boolean rightforward(int speedleft, int speedright) {
        try{
            mc=new UsualMoveCommand(speedleft,speedright, UsualMoveDirection.RFORWARD);
            client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
            sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        }catch (Exception e){
            Log.d("Could not move: ",e.getMessage());
        }
        return true;

    }

    public boolean gyroforward(int speedLeft, int speedRight, float inclinationX, float inclinationY,  ScreenOrientation orientation) throws Exception {
        Log.d("Raw-Instr: FW", ("Gyroforward: Speedl: "+speedLeft+", Speedr: "
                + speedRight + ", InclinationX: "+inclinationX + ", InclinationY: "+inclinationY + ", Orientatioin: "+orientation));

        mc=new GyroMoveCommand(speedLeft,speedRight,orientation, inclinationX, inclinationY, ForwardBackwardDirection.FORWARD);
        client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
        Log.i("Instruction", "SpeedLeft: " + mc.getLeftSpeed() + ", SpeedRight: " + mc.getRightSpeed());
        sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        return true;
    }

    public boolean gyrobackward(int speedLeft, int speedRight,float inclinationX, float inclinationY, ScreenOrientation orientation) throws Exception {
        Log.d("Raw-Instr: FW", ("Gyroforward: Speedl: "+speedLeft+", Speedr: "
                + speedRight + ", InclinationX: "+inclinationX + ", InclinationY: "+inclinationY + ", Orientatioin: "+orientation));

        mc=new GyroMoveCommand(speedLeft,speedRight,orientation, inclinationX, inclinationY, ForwardBackwardDirection.FORWARD);
        client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
        Log.i("Instruction", "SpeedLeft: " + mc.getLeftSpeed() + ", SpeedRight: " + mc.getRightSpeed());
        sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        return true;
    }

    //Stop robot method
    public boolean stop(){
        try{
            mc=new StopMoveCommand();
            client.sendCommand(dataFactory.getCommandData(PanicCommand.class));
            sendCommand(ControlMotorCommand.class, Integer.toString(mc.getLeftSpeed()), Integer.toString(mc.getRightSpeed()));
        }catch (Exception e){
            Log.d("Could not move: ",e.getMessage());
        }
        return true;
    }

    private void listenToServer(String ip) {
        try{
            stopListening();
            client = new BitBotClient(ip);
            client.connectToServer();
            dataFactory = new CommandDataFactory();
            sendCommand(EstablishConnectionCommand.class);
        }
        catch(Exception e){
            Log.d("BitBotException", e.getMessage());
        }
    }
    private void stopListening() {
        if (client != null) {
            if (client.isConnected()) {
                client.closeConnection();
            }
            client = null;
        }
        dataFactory = null;
    }
    private void sendCommand(Class<?> id, String... param) {
        sendCommand(id, new ArrayList<>(Arrays.asList(param)));
    }
    private void sendCommand(Class<?> id, List<String> param) {
        CommandData data = dataFactory.getCommandData(id, param);
        try{
            client.sendCommand(data);
        }catch (Exception e) {
            Log.d("BitBotException", e.getMessage());
        }
    }

}
