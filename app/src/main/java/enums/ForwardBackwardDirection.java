package enums;

/**
 * Created by Jonathan Resch on 11.01.2016.
 */
public enum ForwardBackwardDirection {
    FORWARD, BACKWARD
}
