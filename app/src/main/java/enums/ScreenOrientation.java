package enums;

public enum ScreenOrientation {
    PORTRAIT, LANDSCAPELEFT, LANDSCAPERIGHT
}
