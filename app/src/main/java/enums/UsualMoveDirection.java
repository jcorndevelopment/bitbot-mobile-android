package enums;

/**
 * Created by Jonathan Resch on 11.01.2016.
 */
public enum UsualMoveDirection {
    FORWARD, BACKWARD, LEFT, RIGHT, LFORWARD, RFORWARD
}
