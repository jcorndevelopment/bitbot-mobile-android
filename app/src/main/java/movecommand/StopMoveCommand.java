package movecommand;

public class StopMoveCommand implements MoveCommand{
    @Override
    public int getLeftSpeed() {
        return 0;
    }

    @Override
    public int getRightSpeed() {
        return 0;
    }
}
