package movecommand;

import android.util.Log;

import bl.Backwarder;
import enums.ForwardBackwardDirection;
import enums.ScreenOrientation;

public class GyroMoveCommand implements MoveCommand{
    private int speedLeft, speedRight;
    private ScreenOrientation orientation;
    private float inclinationX, inclinationY;
    private ForwardBackwardDirection direction;

    public GyroMoveCommand(int speedLeft, int speedRight, ScreenOrientation orientation, float inclinationX, float inclinationY, ForwardBackwardDirection direction){
        this.speedLeft=speedLeft;
        this.speedRight=speedRight;
        this.orientation=orientation;
        this.inclinationX=inclinationX;
        this.inclinationY=inclinationY;
        this.direction=direction;
    }

    @Override
    public int getLeftSpeed(){
        float speedleft=0;
        try{
            switch (orientation){
                case LANDSCAPELEFT://Landscape left
                    //if(inclinationY>5)inclinationY=5;
                    //if(inclinationY<-5)inclinationY=-5;

                    if(inclinationY>=0){//right
                        speedleft=speedLeft;
                    }
                    if(inclinationY<0){//left
                        speedleft=((100-(inclinationY*10)*(-1))/100)*speedLeft;//use *20 if you use the upper if statements for more direct steering
                    }
                    break;
                case PORTRAIT://Portrait
                    //if(inclinationX>5)inclinationX=5;
                    //if(inclinationX<-5)inclinationX=-5;

                    if(inclinationX>=0){//left

                        speedleft=((100-(inclinationX*10))/100)*speedLeft;//use *20 if you use the upper if statements for more direct steering
                    }
                    if(inclinationX<0){//right
                        speedleft=speedLeft;
                    }
                    break;
                case LANDSCAPERIGHT://Landscape right
                    //if(inclinationY>5)inclinationY=5;
                    //if(inclinationY<-5)inclinationY=-5;

                    if(inclinationY>=0){//right
                        speedleft=((100-(inclinationY*10))/100)*speedLeft;//use *20 if you use the upper if statements for more direct steering

                    }
                    if(inclinationY<0){//left
                        speedleft=speedLeft;
                    }
                    break;
                default:
                    throw new Exception("Could not resolve orientation status!");
            }
        }catch(Exception e){
            Log.e("Erorr", e.getMessage());
        }
        speedLeft=(int)speedleft;
        if (direction==ForwardBackwardDirection.BACKWARD) speedLeft=Backwarder.getBackwardValue(speedLeft);
        return speedLeft;
    }

    @Override
    public int getRightSpeed() {
        float speedright=0;
        try{
            switch (orientation){
                case LANDSCAPELEFT://Landscape left
                    //if(inclinationY>5)inclinationY=5;
                    //if(inclinationY<-5)inclinationY=-5;

                    if(inclinationY>=0){//right
                        speedright=((100-(inclinationY*10))/100)*speedRight;//use *20 if you use the upper if statements for more direct steering
                    }
                    if(inclinationY<0){//left
                        speedright=speedRight;
                    }
                    break;
                case PORTRAIT://Portrait
                    //if(inclinationX>5)inclinationX=5;
                    //if(inclinationX<-5)inclinationX=-5;

                    if(inclinationX>=0){//left
                        speedright=speedRight;
                    }
                    if(inclinationX<0){//right
                        speedright=((100-(inclinationX*10)*(-1))/100)*speedRight;//use *20 if you use the upper if statements for more direct steering
                    }
                    break;
                case LANDSCAPERIGHT://Landscape right
                    //if(inclinationY>5)inclinationY=5;
                    //if(inclinationY<-5)inclinationY=-5;

                    if(inclinationY>=0){//right
                        speedright=speedRight;

                    }
                    if(inclinationY<0){//left
                        speedright=((100-(inclinationY*10)*(-1))/100)*speedRight;//use *20 if you use the upper if statements for more direct steering
                    }
                    break;
                default:
                    throw new Exception("Could not resolve orientation status!");
            }
        }catch(Exception e) {
            Log.e("Error", e.getMessage());
        }
        speedRight=(int)speedright;
        if (direction==ForwardBackwardDirection.BACKWARD) speedRight=Backwarder.getBackwardValue(speedRight);
        return speedRight;
    }
}
