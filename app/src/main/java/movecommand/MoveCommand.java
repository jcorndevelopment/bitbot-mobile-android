package movecommand;

public interface MoveCommand  {
    int getLeftSpeed();
    int getRightSpeed();
}
;