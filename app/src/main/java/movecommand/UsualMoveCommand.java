package movecommand;

import bl.Backwarder;
import enums.UsualMoveDirection;

public class UsualMoveCommand implements MoveCommand{
    private int speedleft, speedright;
    private UsualMoveDirection direction;

    public UsualMoveCommand(int speedLeft, int speedRight, UsualMoveDirection direction){
        this.speedleft=speedLeft;
        this.speedright=speedRight;
        this.direction=direction;
    }

    @Override
    public int getLeftSpeed() {
        switch (direction){
            case FORWARD:
                return speedleft;
            case BACKWARD:
                return Backwarder.getBackwardValue(speedleft);
            case LEFT:
                return Backwarder.getBackwardValue(speedleft);
            case RIGHT:
                return speedleft;
            case LFORWARD:
                return (int)(speedleft*0.50);
            case RFORWARD:
                return speedleft;
            default:
                return 0;
        }
    }

    @Override
    public int getRightSpeed() {
        switch (direction){
            case FORWARD:
                return speedright;
            case BACKWARD:
                return Backwarder.getBackwardValue(speedright);
            case LEFT:
                return speedright;
            case RIGHT:
                return Backwarder.getBackwardValue(speedright);
            case LFORWARD:
                return speedright;
            case RFORWARD:
                return (int)(speedright*0.50);
            default:
                return 0;
        }
    }
}
