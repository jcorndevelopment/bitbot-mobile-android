package bitbot.pibotcontroll;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Button;
import android.view.*;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import at.petritzdesigns.bitbot.exceptions.BitBotException;
import at.petritzdesigns.bitbot.networking.BitBotClient;
import at.petritzdesigns.bitbot.networking.data.CommandDataFactory;
import at.petritzdesigns.bitbot.networking.data.commands.PanicCommand;
import bl.Sender;
import enums.ScreenOrientation;

public class MainActivity extends AppCompatActivity implements SensorEventListener{

    //Buttons - Movement
    private Button forward;
    private Button backward;
    private Button left;
    private Button right;
    private Button leftforward;
    private Button rightforward;

    //ButtonState forward, backward
    private boolean isForwardPressed=false;
    private boolean isBackwardPressed=false;

    //ToggleButtons
    private ToggleButton togGyro;

    //togglebutton status
    private boolean tobGyroState=false;

    //Buttons - Connect, Disconnect, Emergency Stop
    private Button btemergency;
    private Button btconnect;
    private Button btdisconnect;

    //TextViews and EditTexts for Connection Input
    private EditText tfip, tfport;
    private TextView tvIp, tvPort;

    //SeekBars for the speed
    private SeekBar sbl;
    private SeekBar sbr;

    //textviews to display the seekbars progresses
    private TextView tvspl;
    private TextView tvspr;

    //Boolean for the visability of the connection menu
    private boolean conMenu=false;

    //Boolean for the connection status
    private boolean conStatus=false;

    //int values for the motor speed (seek bars)
    private int leftSpeed=100;
    private int rightSpeed=100;

    //values for the gyroscope
    private float inclinationX;
    private float inclinationY;

    //SensorManager
    private SensorManager sm;

    //Sender
    private Sender sender=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init Buttons and Listeners
        forward=(Button) findViewById(R.id.forward);
        forward.setOnTouchListener(forwardListener);

        backward=(Button)findViewById(R.id.backward);
        backward.setOnTouchListener(backwardListener);

        left=(Button)findViewById(R.id.left);
        left.setOnTouchListener(leftListener);

        right=(Button)findViewById(R.id.right);
        right.setOnTouchListener(rightListener);

        leftforward=(Button)findViewById(R.id.leftforward);
        leftforward.setOnTouchListener(leftforwardListener);

        rightforward=(Button)findViewById(R.id.rightforward);
        rightforward.setOnTouchListener(rightforwardListener);


        btemergency=(Button)findViewById(R.id.emergency);
        btemergency.setOnClickListener(emergencyListener);

        btconnect=(Button)findViewById(R.id.connect);
        btconnect.setOnClickListener(connectListener);

        btdisconnect=(Button)findViewById(R.id.disconnect);
        btdisconnect.setOnClickListener(disconnectListener);

        togGyro=(ToggleButton) findViewById(R.id.tobGyro);
        togGyro.setOnCheckedChangeListener(buttonGyroListener);

        //textviews to display the seekbar-progresses
        tvspl=(TextView) findViewById(R.id.tfPercL);
        tvspl.setText(leftSpeed + "%");
        tvspr=(TextView) findViewById(R.id.tfPercR);
        tvspr.setText(rightSpeed + "%");

        //SeekBars
        sbl=(SeekBar) findViewById(R.id.sbL);
        sbl.setOnSeekBarChangeListener(leftSeekBarChangeListener);
        sbl.setProgress(leftSpeed);

        sbr=(SeekBar) findViewById(R.id.sbR);
        sbr.setOnSeekBarChangeListener(rightSeekBarChangeListener);
        sbr.setProgress(rightSpeed);



        //Init and hide input dialogs for connection
        tfip=(EditText) findViewById(R.id.tfIP);
        tfport=(EditText) findViewById(R.id.tfPort);

        tvIp=(TextView) findViewById(R.id.tvIp);
        tvPort=(TextView) findViewById(R.id.tvPort);

        hideConMenu();

        //Set Gyroscope Listener
        sm=(SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(sm.getSensorList(Sensor.TYPE_ACCELEROMETER).size()!=0){
            Sensor s = sm.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
            sm.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
        }

        inclinationX=inclinationY=0;
    }

    @Override
    protected void onPause() {
        Log.d("Listener: ", "onPause");
        sm.unregisterListener(this);
        isForwardPressed=false;
        if(sender!=null)sender.stop();
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d("Listener: ", "onResume");
        sm=(SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if(sm.getSensorList(Sensor.TYPE_ACCELEROMETER).size()!=0){
            Sensor s = sm.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
            sm.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
        }
        super.onResume();
    }

    //Event Listeners for the Move-Buttons
    private View.OnTouchListener forwardListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    Log.d("Move: ", "forwardPressed");

                    if(sender!=null){
                        if(tobGyroState!=true){
                            sender.forward(leftSpeed,rightSpeed);
                        }
                        else{
                            isForwardPressed=true;
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
                            Thread t = new Thread(){
                                @Override
                                public void run() {
                                    while(isForwardPressed==true){
                                        try {
                                            sender.gyroforward(leftSpeed, rightSpeed, inclinationX,
                                                    inclinationY,  getScreenOrientation());
                                        } catch (Exception e) {
                                            //Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                        try {
                                            sleep(200);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            };
                            t.start();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    isForwardPressed=false;
                    Log.d("Move: ", "forwardReleased");
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                    if(sender!=null){
                        sender.stop();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
            }
            return false;
        }

    };


    private View.OnTouchListener backwardListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    Log.d("Move: ", "backwardPressed");
                    if(sender!=null){
                        if(tobGyroState!=true){
                            sender.backward(leftSpeed,rightSpeed);
                        }
                        else{
                            isBackwardPressed=true;
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
                            Thread t = new Thread(){
                                @Override
                                public void run() {
                                    while(isBackwardPressed==true){
                                        try {
                                            sender.gyrobackward(leftSpeed, rightSpeed, inclinationX,
                                                    inclinationY, getScreenOrientation());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            sleep(200);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            };
                            t.start();
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    Log.d("Move: ", "backwardReleased");
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                    if(sender!=null){
                        isBackwardPressed=false;
                        sender.stop();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
            }
            return false;
        }

    };
    private View.OnTouchListener leftListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    Log.d("Move: ", "leftPressed");
                    if(sender!=null){
                        sender.left(leftSpeed, rightSpeed);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    Log.d("Move: ", "leftReleased");
                    if(sender!=null){
                        sender.stop();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
            }
            return false;
        }

    };
    private View.OnTouchListener rightListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    Log.d("Move: ", "rightPressed");
                    if(sender!=null){
                        sender.right(leftSpeed, rightSpeed);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    Log.d("Move: ", "rightReleased");
                    if(sender!=null){
                        sender.stop();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
            }
            return false;
        }
    };
    private View.OnTouchListener leftforwardListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    Log.d("Move: ", "leftforwardPressed");
                    if(sender!=null){
                        sender.leftforward(leftSpeed, rightSpeed);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    Log.d("Move: ", "leftforwardReleased");
                    if(sender!=null){
                        sender.stop();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
            }
            return false;
        }

    };
    private View.OnTouchListener rightforwardListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch(event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    // PRESSED
                    Log.d("Move: ", "rightforwardPressed");
                    if(sender!=null){
                        sender.rightforward(leftSpeed, rightSpeed);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
                case MotionEvent.ACTION_UP:
                    // RELEASED
                    Log.d("Move: ", "rightforwardReleased");
                    if(sender!=null){
                        sender.stop();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Please connect to a Robot first", Toast.LENGTH_SHORT).show();
                    }
                    return true; // if you want to handle the touch event
            }
            return false;
        }
    };

    //Listeners for the Connection Buttons
    private View.OnClickListener emergencyListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("Emergency: ", "EmergencyClicked");
            emergencyStop();
        }

    };
    private View.OnClickListener connectListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("Connect: ", "ConnectClicked");
            connect();
        }

    };

    private View.OnClickListener disconnectListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("Disconnect: ", "DisconnectClicked");
            disconnect();
            sender=null;
        }

    };

    //Listener for the Toggle Button to enable/disable Gyroscope Controll
    private CompoundButton.OnCheckedChangeListener buttonGyroListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.d("Gyro: ", "GyroToggleButtonPressed");
            if (isChecked) {
                Log.d("Gyro: ", "GyroToggleButton: true");
                tobGyroState=true;
                left.setVisibility(View.INVISIBLE);
                leftforward.setVisibility(View.INVISIBLE);
                right.setVisibility(View.INVISIBLE);
                rightforward.setVisibility(View.INVISIBLE);
            } else {
                Log.d("Gyro: ", "GyroToggleButton: false");
                tobGyroState=false;
                left.setVisibility(View.VISIBLE);
                leftforward.setVisibility(View.VISIBLE);
                right.setVisibility(View.VISIBLE);
                rightforward.setVisibility(View.VISIBLE);
            }
        }

    };

    private SeekBar.OnSeekBarChangeListener leftSeekBarChangeListener=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            leftSpeed=progress;
            tvspl.setText(leftSpeed+"%");
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };
    private SeekBar.OnSeekBarChangeListener rightSeekBarChangeListener=new SeekBar.OnSeekBarChangeListener(){
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            rightSpeed=progress;
            tvspr.setText(rightSpeed+"%");
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };

    private void emergencyStop(){
        if(sender!=null){
            if(sender.stop()){
                Toast.makeText(getApplicationContext(), "Stopped everything", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(getApplicationContext(), "You are not even connected", Toast.LENGTH_SHORT).show();
        }
    }

    private void connect(){
        if(sender==null){
            if(conMenu==false){
                showConMenu();
            }else{
                try{
                    String ip=tfip.getText().toString();
                    int port=Integer.parseInt(tfport.getText().toString());
                    sender=new Sender(ip, port);

                    conStatus=sender.connect();
                    if(conStatus==false){
                        throw new Exception("Could not connect to the network");
                    }
                    Toast.makeText(getApplicationContext(), "Connected!", Toast.LENGTH_SHORT).show();
                    hideConMenu();
                }catch(NumberFormatException e){
                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                    sender=null;
                }catch (Exception ex){
                    Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                    sender=null;
                }
            }
        }else{
            Toast.makeText(getApplicationContext(), "Ok, Ok, you are already connected!", Toast.LENGTH_SHORT).show();
        }

    }

    private void disconnect(){
        if(sender!=null){
            sender.stop();
            conStatus=sender.disconnect();
            if(conStatus!=false){
                Toast.makeText(getApplicationContext(), "Connection still active (Could not disconnect)!", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplicationContext(),"Disconnected!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void hideConMenu(){
        conMenu=false;
        tfip.setVisibility(View.GONE);
        tfport.setVisibility(View.GONE);
        tvIp.setVisibility(View.GONE);
        tvPort.setVisibility(View.GONE);
    }
    private void showConMenu() {
        conMenu = true;
        tfip.setVisibility(View.VISIBLE);
        tfport.setVisibility(View.VISIBLE);
        tvIp.setVisibility(View.VISIBLE);
        tvPort.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        inclinationX=event.values[0];
        inclinationY=event.values[1];

        //Log.d("SensorData", "inclinationX: " + inclinationX);
        //Log.d("SensorData", "inclinationY: " + inclinationY);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //Not needed
    }

    private ScreenOrientation getScreenOrientation() {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        ScreenOrientation orientation;
        // if the device's natural orientation is portrait:
        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width
                    || (rotation == Surface.ROTATION_90
                        || rotation == Surface.ROTATION_270) && width > height) {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ScreenOrientation.PORTRAIT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ScreenOrientation.LANDSCAPELEFT;
                    break;
                case Surface.ROTATION_180:
                    orientation =ScreenOrientation.LANDSCAPERIGHT;
                    break;
                default:
                    Log.e("Orientation", "Unknown screen orientation. Defaulting to " +
                            "portrait.");
                    orientation =ScreenOrientation.PORTRAIT;
                    break;
            }
        }
        // if the device's natural orientation is landscape or if the device
        // is square:
        else {
            switch(rotation) {
                case Surface.ROTATION_0:
                    orientation = ScreenOrientation.LANDSCAPELEFT;
                    break;
                case Surface.ROTATION_90:
                    orientation = ScreenOrientation.PORTRAIT;
                    break;
                case Surface.ROTATION_180:
                    orientation = ScreenOrientation.LANDSCAPERIGHT;
                    break;
                default:
                    Log.e("Orientation", "Unknown screen orientation. Defaulting to " +
                            "landscape.");
                    orientation = ScreenOrientation.PORTRAIT;
                    break;
            }
        }

        return orientation;
    }
}

