package at.petritzdesigns.bitbot.control;

import at.petritzdesigns.bitbot.exceptions.BitBotControlException;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public interface Roboter {

    /**
     * Setup everything to control roboter afterwards
     *
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when something failed
     */
    public boolean setup() throws BitBotControlException;

    /**
     * Shutdowns roboter
     *
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when something failed
     */
    public boolean shutdown() throws BitBotControlException;

    /**
     * Move Roboter Left
     *
     * @param speed Speed of the motor<br>
     * -100 - 0: Backwards<br>
     * 0 - 100: Forwards<br>
     * 0: Stop<br>
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when something failed
     */
    public boolean moveLeft(int speed) throws BitBotControlException;
    
    /**
     * Move Roboter Right
     *
     * @param speed Speed of the motor<br>
     * -100 - 0: Backwards<br>
     * 0 - 100: Forwards<br>
     * 0: Stop<br>
     * @return success
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotControlException when something failed
     */
    public boolean moveRight(int speed) throws BitBotControlException;
}
