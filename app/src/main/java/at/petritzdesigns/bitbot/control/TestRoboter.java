package at.petritzdesigns.bitbot.control;

import at.petritzdesigns.bitbot.exceptions.BitBotControlException;
import java.io.PrintStream;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class TestRoboter implements Roboter {

    /**
     * Printstream to write outputs
     */
    private final PrintStream out;

    /**
     * Default constructor<br>
     * uses default out
     */
    public TestRoboter() {
        this.out = System.out;
    }

    /**
     * Constructor that sets printstream
     *
     * @param out printstream
     */
    public TestRoboter(PrintStream out) {
        this.out = out;
    }

    /**
     * Just outputs method name (testing only)
     *
     * @return always true
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean setup() throws BitBotControlException {
        return true;
    }

    /**
     * Just outputs method name (testing only)
     *
     * @return always true
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean shutdown() throws BitBotControlException {
        return true;
    }

    /**
     * Just outputs method name (testing only)
     *
     * @return always true
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean moveLeft(int speed) throws BitBotControlException {
        return true;
    }

    /**
     * Just outputs method name (testing only)
     *
     * @return always true
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean moveRight(int speed) throws BitBotControlException {
        return true;
    }

}
