package at.petritzdesigns.bitbot.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class FileCreator {

    /**
     * Creates path and file if they do not already exist
     *
     * @param pathStr the path
     * @throws IOException if path is invalid or path cannot be created
     */
    public static void createFileIfNotExist(String pathStr) throws IOException {
        if (pathStr.isEmpty()) {
            throw new IOException("Path is invalid.");
        }
        File path = new File(pathStr);

        if (!path.getParentFile().exists()) {
            path.getParentFile().mkdirs();
        }
        if (!path.exists()) {
            path.createNewFile();
        }
    }

    /**
     * Reads first line from file
     *
     * @param pathStr path
     * @return first line
     */
    public static String readLineFromFile(String pathStr) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(pathStr)));
            if (br.ready()) {
                String ip = br.readLine();
                if (Ipv4AddressHelper.validate(ip)) {
                    return ip;
                }
            }
        } catch (IOException ex) {
            return "";
        }
        return "";
    }

    /**
     * Writes text to file, creates file if not exist
     *
     * @param pathStr path
     * @param text text to write
     */
    public static void writeToFile(String pathStr, String text) {
        try {
            File f = new File(pathStr);
            createFileIfNotExist(pathStr);
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
                bw.write(text);
                bw.flush();
            }
        } catch (Exception ex) {
        }
    }
}
