package at.petritzdesigns.bitbot.utility;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class TimeCreator {

    /**
     * Creates X amount of seconds and returns the milliseconds
     *
     * @param seconds X
     * @return seconds as milliseconds
     */
    public static long createSeconds(int seconds) {
        return seconds * 1000;
    }

    /**
     * Creates X amount of minutes and returns the milliseconds
     *
     * @param minutes X
     * @return minutes as milliseconds
     */
    public static long createMinutes(int minutes) {
        return minutes * 60 * 1000;
    }
}
