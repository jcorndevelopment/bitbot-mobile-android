package at.petritzdesigns.bitbot.utility;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public enum DisplayType {
    SAVE_FILE,
    OPEN_FILE,
    INPUT_TEXT
}
