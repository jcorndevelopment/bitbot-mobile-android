package at.petritzdesigns.bitbot.networking.data;

import java.util.LinkedList;
import java.util.List;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class CommandDataFactory {

    /**
     * Sequence number<br>
     * will be automaticaly increased by default
     */
    private short sequence;

    /**
     * Whether sequence number should be increased or not
     */
    private boolean increaseSequence;

    /**
     * Default constructor<br>
     * sets sequence number to 0
     */
    public CommandDataFactory() {
        this((short) 0);
    }

    /**
     * Constructor<br>
     * sets increase sequence to true
     *
     * @param sequence sequence number
     */
    public CommandDataFactory(short sequence) {
        this(sequence, true);
    }

    /**
     * Constructor
     *
     * @param sequence sequence number
     * @param increaseSequence whether sequence should be automatically
     * increased
     */
    public CommandDataFactory(short sequence, boolean increaseSequence) {
        this.sequence = sequence;
        this.increaseSequence = increaseSequence;
    }

    /**
     * Gets command data with empty parameters
     *
     * @param id the id
     * @return command data
     */
    public CommandData getCommandData(Class<?> id) {
        return getCommandData(id, new LinkedList<String>());
    }

    /**
     * Gets command
     *
     * @param id the id
     * @param parameter the parameter
     * @return command data
     */
    public CommandData getCommandData(Class<?> id, List<String> parameter) {
        if (increaseSequence) {
            sequence++;
        }
        return new CommandData(Commands.getInstance().getIdentifierByClass(id), sequence, parameter);
    }

    /**
     * Get sequence number
     *
     * @return sequence
     */
    public short getSequence() {
        return sequence;
    }

    /**
     * Set sequence number
     *
     * @param sequence number
     */
    public void setSequence(short sequence) {
        this.sequence = sequence;
    }

    /**
     * Automatically increase sequence number
     *
     * @return boolean
     */
    public boolean isIncreaseSequence() {
        return increaseSequence;
    }

    /**
     * Set automatically increase
     *
     * @param increaseSequence boolean
     */
    public void setIncreaseSequence(boolean increaseSequence) {
        this.increaseSequence = increaseSequence;
    }

}
