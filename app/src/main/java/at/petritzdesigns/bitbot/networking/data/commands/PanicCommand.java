package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import java.io.PrintStream;
import java.util.List;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class PanicCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 2<br>
     * Parameter: 0
     */
    public PanicCommand() {
        super((byte) 2, (short) 0);
    }

    /**
     * Calls the robot shutdown method, has high priority in queue
     *
     * @param robot the robot
     * @param out printstream
     * @param params the parameter
     * @throws Exception when something failed
     */
    @Override
    void handle(Roboter robot, PrintStream out, List<String> params) throws Exception {
        //out.println("panic");
        robot.shutdown();
    }

}
