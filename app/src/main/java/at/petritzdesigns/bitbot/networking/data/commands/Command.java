package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.exceptions.WrongParameterCountException;
import java.io.PrintStream;
import java.util.List;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @author Julian Maierl
 * @version 1.0.0
 */
public abstract class Command {

    /**
     * Identifier of the command
     */
    private final byte identifier;

    /**
     * Excepted parameter count of the command
     */
    private final short parameterCount;

    /**
     * Default Constructor
     *
     * @param identifier id
     * @param parameterCount param count
     */
    public Command(byte identifier, short parameterCount) {
        this.identifier = identifier;
        this.parameterCount = parameterCount;
    }

    /**
     * Method that will be executed when the command is called
     *
     * @param robot roboter instance
     * @param out prinstream
     * @param params the parameter
     * @throws Exception when something failed
     */
    abstract void handle(Roboter robot, PrintStream out, List<String> params) throws Exception;

    /**
     * Checks if count is the expected parameter count
     *
     * @param count the real value
     * @return success
     */
    private boolean checkParameterCount(Integer count) {
        return parameterCount == count;
    }

    /**
     * Checks Parameter Count and then calls abstract method: handle
     *
     * @param robot roboter instance
     * @param out prinstream
     * @param params the parameter
     * @throws Exception when something failed
     */
    public void execute(Roboter robot, PrintStream out, List<String> params) throws Exception {
        if (checkParameterCount(params.size())) {
            handle(robot, out, params);
        } else {
            throw new WrongParameterCountException(params.size());
        }
    }

    /**
     * Returns identifier of command
     *
     * @return id
     */
    public byte getIdentifier() {
        return identifier;
    }
}
