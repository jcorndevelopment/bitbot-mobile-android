package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import java.io.PrintStream;
import java.util.List;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class CloseConnectionCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 1<br>
     * Parameter: 0
     */
    public CloseConnectionCommand() {
        super((byte) 1, (short) 0);
    }

    /**
     * Calls the shutdown method of the robot
     *
     * @param robot the robot
     * @param out the printstream
     * @param params the parameter
     * @throws Exception when something failed
     */
    @Override
    void handle(Roboter robot, PrintStream out, List<String> params) throws Exception {
        //out.println("close");
        robot.shutdown();
    }

}
