package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.exceptions.BitBotException;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BBCP {

    /**
     * Regex to identify command
     */
    private static final Pattern COMMAND_PATTERN;

    /**
     * Static initializer
     */
    static {
        COMMAND_PATTERN = Pattern.compile("([0-9]*)\\|([0-9]*)\\|(([\\;a-zA-Z0-9-]*))\\|bb");
    }

    /**
     * Parses command
     *
     * @param line the input
     * @return parsed command data
     * @throws BitBotException when something failed
     */
    public static CommandData parseCommand(String line) throws BitBotException {
        Matcher m = COMMAND_PATTERN.matcher(line);
        if (!m.find()) {
            throw new BitBotException("Wrong Command Pattern: [" + line + "]");
        }

        String idStr = m.group(1);
        String seqStr = m.group(2);
        String paramStr = m.group(3);

        byte id = Byte.parseByte(idStr);
        short seq = Short.parseShort(seqStr);

        String[] param;
        if (!paramStr.isEmpty()) {
            param = paramStr.split(";");
        } else {
            param = new String[0];
        }
        return new CommandData(id, seq, new ArrayList<>(Arrays.asList(param)));
    }

}
