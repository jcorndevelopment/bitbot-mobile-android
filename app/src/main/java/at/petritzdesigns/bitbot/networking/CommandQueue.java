package at.petritzdesigns.bitbot.networking;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.Commands;
import at.petritzdesigns.bitbot.networking.data.commands.Command;
import at.petritzdesigns.bitbot.networking.data.commands.PanicCommand;
import java.io.PrintStream;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @author Julian Maierl
 * @version 1.0.0
 */
public class CommandQueue {

    /**
     * Queue of commands
     */
    private final Queue<CommandData> queue;

    /**
     * Thread should run
     */
    private boolean running;

    /**
     * Thread should execute all then stop
     */
    private boolean wait;

    /**
     * Roboter instance
     */
    private final Roboter robot;

    /**
     * Output instance
     */
    private final PrintStream output;

    /**
     * Executor service for threads
     */
    private final ExecutorService executor;

    /**
     * Runs in background and works through queue
     */
    private final QueueWorker worker;

    /**
     * Constructor<br>
     * Registers default commands<br>
     * Creates empty Queue<br>
     * Builts Executor service with cached thread pool
     *
     * @param robot robot instance
     */
    public CommandQueue(Roboter robot) {
        this(robot, System.out);
    }

    /**
     * Constructor<br>
     * Registers default commands<br>
     * Creates empty queue<br>
     * Builts Executor service with single thread pool
     *
     * @param robot robot instance
     * @param output output instance
     */
    public CommandQueue(Roboter robot, PrintStream output) {
        this.robot = robot;
        this.output = output;
        this.queue = new LinkedBlockingQueue<>();
        this.executor = Executors.newSingleThreadExecutor();
        this.worker = new QueueWorker();
        this.running = false;
        this.wait = false;
    }

    /**
     * Starts working through queue
     */
    public void start() {
        running = true;
        executor.execute(worker);
    }

    /**
     * Waits for every command in the queue to execute and then shutdowns queue
     *
     * @throws InterruptedException when something failed
     */
    public void await() throws InterruptedException {
        wait = true;
        executor.shutdown();
    }

    /**
     * Shutdowns queue immediately
     */
    public void stop() {
        running = false;
        executor.shutdown();
    }

    /**
     * Adds command data to the queue<br>
     * Shutdowns everything if a panic command is sent
     *
     * @param data command data
     */
    public void add(CommandData data) {
        if (data.getIdentifier() == Commands.getInstance().getIdentifierByClass(PanicCommand.class)) {
            queue.clear();
            stop();
        }
        queue.add(data);
    }

    /**
     * Background thread that works through the queue
     */
    private class QueueWorker implements Runnable {

        @Override
        public void run() {
            while (running) {
                try {
                    CommandData data = queue.poll();
                    if (data != null) {
                        if (Commands.getInstance().getRegisteredCommands().containsKey(data.getIdentifier())) {
                            Command command = Commands.getInstance().getRegisteredCommands().get(data.getIdentifier());
                            command.execute(robot, output, data.getParameter());
                        }
                    } else if (wait) {
                        break;
                    }
                    Thread.sleep(100);
                } catch (Exception ex) {
                    System.err.println(ex);
                    if (wait) {
                        break;
                    }
                }
            }
        }

    }
}
