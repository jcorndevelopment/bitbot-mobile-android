package at.petritzdesigns.bitbot.networking.data;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public enum ServerPort {
    /**
     * Default BitBot Server Port
     */
    DEFAULT_PORT(6969),
    /**
     * Default BitBot Simulation Ports
     */
    SIMULATION_PORT(7272);

    private final Integer port;

    private ServerPort(Integer port) {
        this.port = port;
    }

    public Integer getPort() {
        return port;
    }
}
