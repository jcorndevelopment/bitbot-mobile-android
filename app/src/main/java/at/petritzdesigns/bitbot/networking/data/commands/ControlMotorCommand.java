package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import java.io.PrintStream;
import java.util.List;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ControlMotorCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 3<br>
     * Parameter: 2
     */
    public ControlMotorCommand() {
        super((byte) 3, (short) 2);
    }

    /**
     * Calls the Robot Methods to move left and right
     *
     * @param robot the robot
     * @param out printstream
     * @param params the parameter
     * @throws Exception when something failed
     */
    @Override
    void handle(Roboter robot, PrintStream out, List<String> params) throws Exception {
        //out.format("control %s %s\n", params.get(0), params.get(1));

        int speedLeft = Integer.parseInt(params.get(0));
        int speedRight = Integer.parseInt(params.get(1));

        robot.moveLeft(speedLeft);
        robot.moveRight(speedRight);
    }

}
