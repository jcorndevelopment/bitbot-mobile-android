package at.petritzdesigns.bitbot.networking.data.commands;

import at.petritzdesigns.bitbot.control.Roboter;
import java.io.PrintStream;
import java.util.List;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class EstablishConnectionCommand extends Command {

    /**
     * Default Constructor<br>
     * ID: 0<br>
     * Parameter: 0
     */
    public EstablishConnectionCommand() {
        super((byte) 0, (short) 0);
    }

    /**
     * Calls the robot setup method
     *
     * @param robot the robot
     * @param out printstream
     * @param params the parameter
     * @throws Exception when something failed
     */
    @Override
    void handle(Roboter robot, PrintStream out, List<String> params) throws Exception {
        //out.println("establish");
        robot.setup();
    }

}
