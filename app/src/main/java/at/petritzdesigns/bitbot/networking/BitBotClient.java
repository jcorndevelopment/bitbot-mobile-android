package at.petritzdesigns.bitbot.networking;

import android.util.Log;

import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import at.petritzdesigns.bitbot.networking.data.CommandData;
import at.petritzdesigns.bitbot.networking.data.ServerPort;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.channels.DatagramChannel;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotClient {

    /*
    BitBot Client für Android App und Java Programm. Verbindet sich mit Standard
    Port wenn keiner angegeben sonst kann man den Port angeben, hilfreich für
    den Simulator.
    
    Client sollte nur über UDP & BBCP funktionieren.
     */
    /**
     * Timeout for checking if host is reachable
     */
    private static final int TIMEOUT = 1000;

    /**
     * Socket to send the data to the server
     */
    private DatagramSocket socket;

    /**
     * Port which client should listen to
     */
    private final int port;

    /**
     * IP adress of the server
     */
    private final String ip;

    /**
     * Whether client is connected to server or not
     */
    private boolean connected;

    /**
     * Default constructor, uses default port
     *
     * @param ip
     */
    public BitBotClient(String ip) {
        this(ip, ServerPort.DEFAULT_PORT.getPort());
    }

    /**
     * Constructor listens to local server with simulation port
     */
    public BitBotClient() {
        this("127.0.0.1", ServerPort.SIMULATION_PORT.getPort());
    }

    /**
     * Uses custom port
     *
     * @param ip the ip adress
     * @param port custom port
     */
    public BitBotClient(String ip, Integer port) {
        this.ip = ip;
        this.port = port;
    }

    /**
     * Connects to the server
     *
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException
     */
    public void connectToServer() throws BitBotServerException, IOException {
        try {
            InetAddress address = InetAddress.getByName(ip);
            //socket = new DatagramSocket();

            DatagramChannel channel = DatagramChannel.open();
            socket = channel.socket();

            socket.bind(null);
            socket.connect(new InetSocketAddress(address, port));
            connected = true;
        } catch (SocketException ex) {
            throw new BitBotServerException(ex, "Cannot connect to server!");
        } catch (UnknownHostException ex) {
            throw new BitBotServerException(ex, "Cannot connect to server!");
        }
    }

    /**
     * Closes connection to the server
     */
    public void closeConnection() {
        socket.disconnect();
        socket.close();
        connected = false;
    }

    /**
     * Sends the command to the server
     *
     * @param data The Command to send
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException
     */
    public void sendCommand(CommandData data) throws BitBotServerException {
        try {
            InetAddress address = InetAddress.getByName(ip);
            if (!connected) {
                throw new BitBotServerException("Not connected!");
            }
            if (data == null) {
                throw new BitBotServerException("Command is null!");
            }
            byte[] sendData = data.toString().getBytes();
            socket.send(new DatagramPacket(sendData, sendData.length, address, port));
        } catch (IOException ex) {
            throw new BitBotServerException(ex, "Could not send command to server!");
        }
    }

    /**
     * Returns port
     *
     * @return port
     */
    public Integer getPort() {
        return port;
    }

    /**
     * Returns ip adress
     *
     * @return ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Whether client is connected to a server or not
     *
     * @return connected
     */
    public boolean isConnected() {
        return connected;
    }
}
