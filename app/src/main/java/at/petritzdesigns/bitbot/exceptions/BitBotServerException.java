package at.petritzdesigns.bitbot.exceptions;

/**
 * BitBotLibrary
 *
 * @author Julian Maierl
 * @version 1.0.0
 */
public class BitBotServerException extends BitBotException {

    private final Exception initialException;

    /**
     * Creates a new instance of <code>BitBotServerException</code> without
     * detail message.
     */
    public BitBotServerException() {
        this.initialException = null;
    }

    /**
     * Constructs an instance of <code>BitBotServerException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public BitBotServerException(String msg) {
        super(msg);
        this.initialException = null;
    }

    /**
     * Constructor with the initial exception
     *
     * @param initialException initial exception
     */
    public BitBotServerException(Exception initialException) {
        this.initialException = initialException;
    }

    /**
     * Constructor with the initial exception and the message
     *
     * @param initialException initial exception
     * @param msg message
     */
    public BitBotServerException(Exception initialException, String msg) {
        super(msg);
        this.initialException = initialException;
    }

    /**
     * Has initial exception
     *
     * @return boolean
     */
    public boolean hasInitialException() {
        return initialException != null;
    }

    /**
     * Return initial exception
     *
     * @return exception
     */
    public Exception getInitialException() {
        return initialException;
    }
}
