package at.petritzdesigns.bitbot.exceptions;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotException extends Exception {

    /**
     * Empty Constructor
     */
    public BitBotException() {
    }

    /**
     * Constructor with message
     *
     * @param msg the message
     */
    public BitBotException(String msg) {
        super(msg);
    }

    /**
     * Returns message
     *
     * @return message
     */
    @Override
    public String getMessage() {
        if (!super.getMessage().isEmpty()) {
            return super.getMessage();
        }
        return super.toString();
    }
}
