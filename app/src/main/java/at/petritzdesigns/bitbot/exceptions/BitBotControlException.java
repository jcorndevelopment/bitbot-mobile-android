package at.petritzdesigns.bitbot.exceptions;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotControlException extends BitBotException {

    /**
     * Empty Constructor
     */
    public BitBotControlException() {
    }

    /**
     * Constructor with message
     *
     * @param msg the message
     */
    public BitBotControlException(String msg) {
        super(msg);
    }
}
