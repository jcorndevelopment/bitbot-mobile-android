package at.petritzdesigns.bitbot.exceptions;

/**
 * BitBotLibrary
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class WrongParameterCountException extends BitBotException {

    /**
     * Message of the Exception
     */
    private final String msg;

    /**
     * Constructor with parameter count
     *
     * @param count parameter count
     */
    public WrongParameterCountException(Integer count) {
        this.msg = "Wrong Parameter Count: [" + count + "]";
    }

    /**
     * Constructor with custom message
     *
     * @param msg the message
     */
    public WrongParameterCountException(String msg) {
        this.msg = msg;
    }

    /**
     * Returns the message
     *
     * @return message
     */
    @Override
    public String getMessage() {
        return msg;
    }
}
